﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Trades.Domain;

namespace Trades
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Trade> trades = new List<Trade>();

            Console.WriteLine("Credit Suisse Bank.");
            Console.WriteLine("Inputing trade. Please, type");
            Console.WriteLine();
            Console.Write("Reference date: ");
            var referenceDate = DateTime.Parse(Console.ReadLine());
            Console.Write("Quantity of trades: ");
            var tradeQuantity = int.Parse(Console.ReadLine());

            try
            {
                for (int i = 1; i <= tradeQuantity; i++)
                {
                    Console.Write($"Trade number #{i}: ");
                    var userTradeInput = Console.ReadLine();
                    string[] userTrade = userTradeInput.Split(' ');

                    Trade trade = new Trade();
                    trade.Value = double.Parse(userTrade[0], CultureInfo.InvariantCulture);
                    trade.ClientSector = userTrade[1];
                    trade.NextPaymentDate = DateTime.Parse(userTrade[2], new CultureInfo("en-US"));
                    trade.SetCategory(referenceDate);
                    trades.Add(trade);
                }

                Console.WriteLine();
                Console.WriteLine("Result:");

                foreach (var trade in trades)
                {
                    Console.WriteLine(trade.ToString());
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Some error ocurred. Please, review your trade or contact IT Team.");
            }
            Console.ReadLine();
        }
    }
}

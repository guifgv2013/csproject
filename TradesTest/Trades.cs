using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Globalization;
using Trades.Domain;
using Trades.Enum;

namespace TradesTest
{
    [TestClass]
    public class Trades
    {
        [TestMethod]
        public void SetCategory_PassingTradeHighRisk_ExpectHIGHRISKCategory()
        {
            Trade trade = new Trade() {Value= 2000000, ClientSector = "Private", NextPaymentDate = DateTime.Parse("12/29/2025", new CultureInfo("en-US")) };
            trade.SetCategory(DateTime.Parse("12/11/2020"));

            Assert.AreEqual(trade.Category, CategoryType.HIGHRISK);
        }

        [TestMethod]
        public void SetCategory_PassingTradeExpired_ExpectEXPIREDCategory()
        {
            Trade trade = new Trade() { Value = 400000, ClientSector = "Public", NextPaymentDate = DateTime.Parse("07/01/2020", new CultureInfo("en-US")) };
            trade.SetCategory(DateTime.Parse("12/11/2020"));

            Assert.AreEqual(trade.Category, CategoryType.EXPIRED);
        }

        [TestMethod]
        public void SetCategory_PassingTradeMediumRisk_ExpectMEDIUMRISKCategory()
        {
            Trade trade = new Trade() { Value = 5000000, ClientSector = "Public", NextPaymentDate = DateTime.Parse("01/02/2024", new CultureInfo("en-US")) };
            trade.SetCategory(DateTime.Parse("12/11/2020"));

            Assert.AreEqual(trade.Category, CategoryType.MEDIUMRISK);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "It's not possible to Set other category other than EXPIRED - MEDIUMRISK - HIGHRISK")]
        public void SetCategory_PassingTradeNotCategorized_ExpectError()
        {
            //Check the value of the trade
            Trade trade = new Trade() { Value = 500, ClientSector = "Public", NextPaymentDate = DateTime.Parse("01/02/2024", new CultureInfo("en-US")) };
            trade.SetCategory(DateTime.Parse("12/11/2020"));

            Assert.AreEqual(trade.Category, CategoryType.MEDIUMRISK);
        }
    }
}

﻿using System;
using Trades.Enum;

namespace Trades.Domain
{
    public class Trade : ITrade
    {
        public double Value { get; set; }

        public string ClientSector { get; set; }

        public DateTime NextPaymentDate { get; set; }

        public CategoryType Category { get; private set; } //It can only be defined internally

        public void SetCategory(DateTime referenceDate)
        {
            if (NextPaymentDate.AddDays(30) < referenceDate)
            {
                Category = CategoryType.EXPIRED;
            }
            else if (Value > 1000000 && ClientSector == ClientSectors.privateSector)
            {
                Category = CategoryType.HIGHRISK;
            }
            else if (Value > 1000000 && ClientSector == ClientSectors.publicSector)
            {
                Category = CategoryType.MEDIUMRISK;
            }
            else
            {
                //TODO: Ask if there is the low risk category
                throw new ArgumentOutOfRangeException();
            }
            
        }

        public override string ToString()
        {
            return Category.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trades.Domain
{
    public static class ClientSectors
    {
        public const string publicSector = "Public";
        public const string privateSector = "Private";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trades.Enum
{
    public enum CategoryType
    {
        EXPIRED = 1,
        HIGHRISK = 2,
        MEDIUMRISK = 3
    }
}
